{
  description = "Attempt havin a beautiful devenv for xpctl";

  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/23.05;
  };

  outputs = { self, nixpkgs }: 
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
  in {
    devShells.${system}.default = with pkgs; mkShell rec {
      name = "impure-python";
      venvDir = "/home/jon/dev/hnrm-experiments/.wt/refactor-config/g5k/kameleon/steps/data/xpctl/ici/";
      packages = [ python3Packages.pip python3Packages.venvShellHook];
      postVenvCreation = ''
        pip install --editable
      '';
      postShellHook = ''
        source env.sh
      '';
      };
  };
}
